var Product = require('../models/product');
var mongoose = require('mongoose');
mongoose.connect('127.0.0.1:27017/shopping');

var products = [
  new Product(
    {
      imagePath: "https://pixabay.com/static/uploads/photo/2016/09/28/23/04/duck-1701766__180.jpg",
      title: 'Gothic 1',
      description: 'Awsome Game 1',
      price: 10
    }),
  new Product(
    {
      imagePath: "https://pixabay.com/static/uploads/photo/2016/09/28/23/04/duck-1701766__180.jpg",
      title: 'Gothic 2',
      description: 'Awsome Game 2',
      price: 15
    }),
  new Product(
    {
      imagePath: "https://pixabay.com/static/uploads/photo/2016/09/28/23/04/duck-1701766__180.jpg",
      title: 'Gothic 3',
      description: 'Awsome Game 3',
      price: 20
    }),
  new Product(
    {
      imagePath: "https://pixabay.com/static/uploads/photo/2016/09/28/23/04/duck-1701766__180.jpg",
      title: 'Gothic 4',
      description: 'Awsome Game 4',
      price: 30
    })
];
var done = 0;
for (var i = 0; i < products.length; i++) {
  products[i].save(function(err, result) {
    done++;
    if (done === products.length) {
      exit();
    }
  });
}
function exit() {
  mongoose.disconnect();
}
